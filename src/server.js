import Koa from "koa";
import Cors from "@koa/cors";
import { ApolloServer } from "apollo-server-koa";

import { typeDefs, resolvers } from "./graphql/schema.js";

const PORT = 3000;

const server = new ApolloServer({
  typeDefs,
  resolvers
});

const app = new Koa();
app.use(Cors());

server.applyMiddleware({ app });

app.listen({ port: PORT }, () =>
  console.log(
    `🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`
  ),
);

export default app;
